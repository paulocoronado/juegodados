from lanzarDados import jugar
from verificarJugada import  verificarJugada
from eliminarCasillas import eliminarCasillas
from concluirJuego import concluirJuego

continuar="S"
while (continuar=="S"):
    posible=True
    casillas = list(range(1, 10))

    while(posible==True):

        juego=jugar()
        posible = verificarJugada(casillas, juego)
        total=0
        while (total<juego and posible==True):
            casillas,total=eliminarCasillas(casillas,total,juego)

    puntaje, casillasDisponibles=concluirJuego(casillas)
    print("Usted ha obtenido un puntaje de:", puntaje, " y ha quedado con ",casillasDisponibles, " casillas abiertas.")
    continuar=input("Desea continuar (S/N)?")




