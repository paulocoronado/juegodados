def ingresarSeleccion():
    seleccion = -1
    while(seleccion<0 or seleccion>9):
        seleccion = int(input())
        if(seleccion<0 or seleccion>9):
            print("Selección no válida. Por favor ingrese un número de 1 a 9")
    return seleccion
