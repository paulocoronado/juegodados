from lanzarDado import lanzarDado


def jugar():

    dado1=lanzarDado()
    dado2=lanzarDado()
    suma=dado1+dado2

    print("Dado 1: ",dado1)
    print("Dado 2: ", dado2)
    print("Total: ", suma)

    return suma