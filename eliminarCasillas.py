from verificarSeleccion import verificarSeleccion
from ingresarSeleccion import ingresarSeleccion

def eliminarCasillas(casillas,total,juego):
    print("Seleccione la casilla que desea eliminar:")
    print(casillas)
    # Ingresar un número entre 1 y 9
    casilla = ingresarSeleccion()
    # Validar que el número esté en el arreglo (no haya sido "tapado")
    validar = verificarSeleccion(casillas, casilla)
    if (validar == True):
        total = total + casilla
        if (total > juego):
            print("La suma de las casillas elegidas debe ser menor a:",juego)
            total = total - casilla
        else:
            casillas[casilla - 1] = 'X'
    return  casillas,total